# Svíčková na smetaně - Recept v MkDocs

Tento projekt obsahuje recept na tradiční české jídlo - svíčkovou na smetaně, připravený pro použití s MkDocs.

## K čemu projekt slouží

Tento projekt slouží jako interaktivní dokumentace receptu na svíčkovou na smetaně. Pomocí MkDocs můžete jednoduše procházet ingredience a postup přípravy tohoto tradičního českého jídla.

## Jak se používá

Projekt obsahuje tři hlavní soubory, které můžete procházet pomocí MkDocs:

- `index.md`: Úvod do receptu, historie a přehled toho, co budeme vařit.
- `ingredience.md`: Seznam ingrediencí potřebných pro přípravu jídla.
- `postup.md`: Detailní postup přípravy krok za krokem.

## Instalace

### Prerekvizity

Pro spuštění tohoto projektu budete potřebovat:

- Python 3.9
- MkDocs

### Stahování projektu z GitLabu

1. **Klonujte repozitář** z GitLabu do vašeho lokálního počítače:

    ```sh
    git clone https://gitlab.com/Ziegto/velka-prace.git
    cd svickova-recept
    ```

2. **Nainstalujte MkDocs** pokud jej ještě nemáte nainstalovaný:

    ```sh
    pip install mkdocs
    ```

3. **Ověřte strukturu projektu**, která by měla vypadat následovně:

    ```sh
    mkdocs.yml
    docs/
        index.md
        ingredience.md
        postup.md
    README.md
    ```

4. **Vytvořte konfigurační soubor `mkdocs.yml`** s následujícím obsahem, pokud již neexistuje:

    ```yaml
    site_name: Recept na svíčkovou na smetaně
    nav:
        - Úvod: index.md
        - Ingredience: ingredience.md
        - Postup: postup.md
    theme: readthedocs
    ```

## Spuštění projektu

1. **Spusťte místní server** pro zobrazení dokumentace:

    ```sh
    mkdocs serve
    ```

2. **Otevřete prohlížeč** a přejděte na `http://127.0.0.1:8000/` pro zobrazení vaší dokumentace.

## Obsah dokumentace

### index.md

Obsahuje úvod do receptu na svíčkovou na smetaně, včetně krátké historie a přehledu toho, co budeme vařit.

### ingredience.md

Zahrnuje kompletní seznam ingrediencí potřebných pro přípravu svíčkové na smetaně, včetně masa, zeleniny, koření a přílohy.

### postup.md

Detailní postup přípravy svíčkové na smetaně krok za krokem, včetně marinování masa, přípravy zeleniny, vaření masa a přípravy omáčky.

## GitLab CI/CD

Tento projekt je nastaven pro automatické lintování, kontrolu syntaxe, build a nasazení pomocí GitLab CI/CD.
