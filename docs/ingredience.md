# Ingredience

## Maso a marináda

- 1 kg hovězího masa (svíčková nebo jiný druh kvalitního masa)
- 2 lžíce oleje
- 2 lžíce octa
- 1 lžička soli
- 1 lžička pepře
- 2 bobkové listy
- 4 kuličky nového koření
- 4 kuličky pepře

## Zelenina a omáčka

- 2 mrkve
- 1 petržel
- 1/4 celeru
- 2 cibule
- 2 lžíce másla
- 2 lžíce hladké mouky
- 200 ml smetany ke šlehání
- 1 lžička cukru
- 1 lžíce citronové šťávy
- 1 lžička soli
- 1 lžička pepře

## Příloha

- Houskový knedlík (lze zakoupit nebo připravit doma)
