# Svíčková na smetaně

Svíčková na smetaně je jedním z nejoblíbenějších a nejtradičnějších českých jídel. Tento pokrm se skládá z hovězího masa, které je podáváno s lahodnou smetanovou omáčkou, zeleninou a houskovým knedlíkem.

## Historie

Svíčková na smetaně má své kořeny v české kuchyni a je známá již od 19. století. Původně byla připravována z hovězí svíčkové, což je nejlepší část hovězího masa. Dnes se tento pokrm často připravuje z jiných částí hovězího, jako je například plec nebo zadní maso. Tradičně se podává při slavnostních příležitostech a rodinných setkáních.

## Co budeme vařit

Budeme připravovat svíčkovou na smetaně s hovězím masem, zeleninou, kořením a smetanou. Tento recept je pro 4 osoby a zahrnuje detailní postup, jak dosáhnout perfektního výsledku.

Následující části obsahují seznam ingrediencí a podrobný postup přípravy tohoto výjimečného jídla.
